//
//  Intrest.hpp
//  Intrest Cal
//
//  Created by Ryan Ferry on 12/27/15.
//  Copyright © 2015 Ryan Ferry. All rights reserved.
//

#ifndef Intrest_hpp
#define Intrest_hpp

#include <stdio.h>

class Intrest {
public:
    double FindIntresCompoundGrowth(double Principel, double years, double rate, double nCompounded);
    double FindIntresCompoundDecay(double Principel, double years, double rate, double nCompounded);
    
private:
    double gIntrest = 0;
    double dIntrest = 0;
    
};

#endif /* Intrest_hpp */
