//
//  main.cpp
//  Intrest Cal
//
//  Created by Ryan Ferry on 12/27/15.
//  Copyright © 2015 Ryan Ferry. All rights reserved.
//

#include <iostream>
#include "Intrest.hpp"
using namespace std;


int main() {
    Intrest i;
    double p,r,t,n;
    
    cout << "Enter the principle: ";
    cin >> p;
    
    cout << "Enter the rate: ";
    cin >> r;
    
    cout << "Enter the time: ";
    cin >> t;
    
    cout << "Enter the number of times it is compounded: ";
    cin >> n;

    cout << "Growth: " << i.FindIntresCompoundGrowth(p, t, r, n) << endl;
    cout << "Decay: " << i.FindIntresCompoundDecay(p, t, r, n) << endl;
    
    return 0;
}


//double Principel, double years, double rate, double nCompounded