//
//  Intrest.cpp
//  Intrest Cal
//
//  Created by Ryan Ferry on 12/27/15.
//  Copyright © 2015 Ryan Ferry. All rights reserved.
//

#include "Intrest.hpp"
#include <iostream>
#include <cmath>

using namespace std;


double Intrest::FindIntresCompoundGrowth(double Principel, double years, double rate, double nCompound){
    double n = 0;
    rate = rate / 100;
    n = nCompound + years;
    gIntrest = pow((1+(rate/nCompound)), n) * Principel;
    return gIntrest;
}

double Intrest::FindIntresCompoundDecay(double Principel, double years, double rate, double nCompounded){
    double n = 0;
    rate = rate / 100;
    n = nCompounded + years;
    dIntrest = pow((1-(rate/nCompounded)), n) * Principel;
    return dIntrest;
}

