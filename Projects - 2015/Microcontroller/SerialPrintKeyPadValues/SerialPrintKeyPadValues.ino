// By Ryan F
// Date Completed: Dec 13, 15
// This project allows you to read the keys that are pressed from a matrix keypad connected to an Arduino 

#include "Keypad.h"

const byte Rows = 4;
const byte Columns = 3;

byte RowPin[Rows] = {8,7,6,5};
byte ColumnsPin[Columns] = {4,3,2};

// Map the keys 
char KeyMap[Rows][Columns] = {{'1','2','3'},
	 						  {'4','5','6'},
	 						  {'7','8','9'},
							  {'*','0','#'}};

//initialize keypad class
Keypad key = Keypad(makeKeymap(KeyMap), RowPin, ColumnsPin, Rows, Columns);

void setup(){
	Serial.begin(9600);
}
void loop(){
	char keypress;
	
	keypress = key.getKey();
	if(keypress != NO_KEY){
		Serial.println(keypress);
	}
}