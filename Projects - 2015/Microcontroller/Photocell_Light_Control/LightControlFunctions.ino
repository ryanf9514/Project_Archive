
//Ryan Ferry 2015

#include <Adafruit_NeoPixel.h>
#include <avr/power.h>

#define PIN 6
Adafruit_NeoPixel strip = Adafruit_NeoPixel(60, PIN, NEO_GRB + NEO_KHZ800);
int photocellPin = 1;     // the cell and 10K pulldown are connected to a0
int photocellReading;     // the analog reading from the analog resistor divider
 
void setup(void) {
	// We'll send debugging information via the Serial monitor
	Serial.begin(9600);   
	strip.begin(); // initialize strip
  	strip.show(); // Initialize all pixels to 'off'
}
 
void loop(void) {
	int a = 0;
  	photocellReading = analogRead(photocellPin);  
 
  	Serial.print("Analog reading = ");
  	Serial.print(photocellReading);     // the raw analog reading
	tempcolor(photocellReading);
  	
 	delay(1000);
}
//displays the light based off of the color
void tempcolor(int light){
	int a = 0;
  	if (photocellReading < 100) {
    	Serial.println(" - Dark");
    	for(int i = 0;i<60;i++){
			strip.setPixelColor(a, 255, 0, 0);	//red
			a = a + 1;
			strip.show();
		}
  	} 
	else if (photocellReading < 200) {
    	Serial.println(" - Dim");
    	for(int i = 0;i<60;i++){	
			strip.setPixelColor(a, 0, 255, 0);	//green
			a = a + 1;
			strip.show();
		}
	}
	else if (photocellReading < 500) {
    	Serial.println(" - Light");
    	for(int i = 0;i<60;i++){
			strip.setPixelColor(a, 0, 0, 255);	//blue
			a = a + 1;
			strip.show();
		}
  }
	else if (photocellReading < 800) {
    	Serial.println(" - Bright");
    	for(int i = 0;i<60;i++){
			strip.setPixelColor(a, 255, 255, 0); //yellow
			a = a + 1;
			strip.show();
		}
	} 
  	else{
    	Serial.println(" - Very bright");
    	for(int i = 0;i<60;i++){
			strip.setPixelColor(a, 255, 255, 255);	//white
			a = a + 1;
			strip.show();
		}
	}
}
