//Ryan Ferry 2015

#define dot 2
#define AA 9
#define BB 8
#define CC 3
#define DD 4
#define EE 5
#define FF 10
#define GG 11
//Seven Segment display is a common A
void setup() {
  pinMode(dot, OUTPUT);
  pinMode(AA, OUTPUT);
  pinMode(BB, OUTPUT);
  pinMode(CC, OUTPUT);
  pinMode(DD, OUTPUT);
  pinMode(EE, OUTPUT);
  pinMode(FF, OUTPUT);
  pinMode(GG, OUTPUT);
  pinMode(7, INPUT);

}

void CLEAR() {
  int Clear[] = {9,2, 8, 3, 4, 5, 10, 11};
  for (int i = 0; i <= 8; i++) {
    digitalWrite(Clear[i], HIGH);
  }
}

void Zero() {
  int zero[] = {2, 9, 8, 3, 4, 5, 10};
  CLEAR();
  for (int i = 0; i <= 7; i++) {
    digitalWrite(zero[i], LOW);
  }
}

void One() {
  int one[] = {2, 8, 3};
  CLEAR();
  for (int i = 0; i <= 3; i++) {
    digitalWrite(one[i], LOW);
  }
}

void Two() {
  int two[] = {9, 8, 11, 5, 4, 2};
  CLEAR();
  for (int i = 0; i <= 6; i++) {
    digitalWrite(two[i], LOW);
  }
}

void Three() {
  int three[] = {9, 8, 11, 3, 4, 2};
  CLEAR();
  for (int i = 0; i <= 6; i++) {
    digitalWrite(three[i], LOW);
  }
}

void Four() {
  int four[] = {2,10,11,8,3};
  CLEAR();
  for (int i = 0; i <= 5; i++) {
    digitalWrite(four[i], LOW);
  }
}

void Five() {
  int five[] = {2, 9, 10, 11, 3, 4};
  CLEAR();
  for (int i = 0; i <= 6; i++) {
    digitalWrite(five[i], LOW);
  }
}

void Six() {
  int six[] = {2, 9, 10,11,3,5,4};
  CLEAR();
  for (int i = 0; i <= 7; i++) {
    digitalWrite(six[i], LOW);
  }
}

void Seven() {
  int seven[] = {9,8,3,2};
  CLEAR();
  for (int i = 0; i <= 4; i++) {
    digitalWrite(seven[i], LOW);
  }
}

void Eight() {
  int eight[] = {2, 9, 8, 3, 4, 5, 10, 11};
  CLEAR();
  for (int i = 0; i <= 8; i++) {
    digitalWrite(eight[i], LOW);
  }
}

void Nine() {
  int eight[] = {9,8,10,11,3,2};
  CLEAR();
  for (int i = 0; i <= 6; i++) {
    digitalWrite(eight[i], LOW);
  }
}

void FlipNumForward() {
  Zero();
  delay(1000);

  One();
  delay(1000);

  Two();
  delay(1000);

  Three();
  delay(1000);

  Four();
  delay(1000);

  Five();
  delay(1000);

  Six();
  delay(1000);

  Seven();
  delay(1000);

  Eight();
  delay(1000);

  Nine();
  delay(1000);
}

void FlipNumReversed() {
  Nine();
  delay(1000);

  Eight();
  delay(1000);

  Seven();
  delay(1000);

  Six();
  delay(1000);

  Five();
  delay(1000);

  Zero();
  delay(1000);

  Four();
  delay(1000);

  Three();
  delay(1000);

  Two();
  delay(1000);

  One();
  delay(1000);
}

void loop() {
  int forward = 0, reversed = 0;
  forward = digitalRead(7);
  CLEAR();
  if(forward == 0){
      FlipNumForward();
      CLEAR();
  }


}
