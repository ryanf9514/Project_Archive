#Ryan Ferry 2015
import Adafruit_BBIO.ADC as ADC
import time

sensor_pin = 'P9_40'
ADC.setup()


def tempInC(mvolts):
    C = (mvolts - 500) / 10
    return C

def mvoltsMath(read):
    M = read * 1800
    return M

def tempInF(TempC):
    F = (TempC * 9/5) + 32
    return F

def fileIO(mvolts,temp_c,temp_f):
    file = open("tempdata.txt","a")
    localtime = time.asctime( time.localtime(time.time()) )
    string = 'MVolts = %d Temp_C = %d Temp_F = %d ---- Time = %s \n' % (mvolts, temp_c, temp_f,localtime)
    file.write(string)


while True:
    reading = ADC.read(sensor_pin)
    mvolts = mvoltsMath(reading)
    temp_c = tempInC(mvolts)
    temp_f = tempInF(temp_c)
    print('MVolts = %d Temp_C = %d Temp_F = %d' % (mvolts, temp_c, temp_f))
    fileIO(mvolts,temp_c,temp_f)
    time.sleep(60)
