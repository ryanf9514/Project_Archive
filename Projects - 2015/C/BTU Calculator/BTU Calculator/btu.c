//
//  main.c
//  Btu calculations
    //calculates how many BTUs of heat are delivered to a house given the number of gallons of oil burned and the efficiency of the house’s oil furnace. Assume that a barrel of oil (42 gallons) has an energy equivalent of 5,800,000 BTU. ( Note:  This number is too large to represent as an int  on some personal computers.) For one test use an efficiency of 65 percent and 100 gallons of oil.
    //The British Thermal Unit, or BTU, is an energy unit. It is approximately the energy needed to heat one pound of water for 1 Fahrenheit. 1 BTU = 1,055 joules. 1BTU/hour = 0.293 watt.
//  Created by Ryan Ferry on 2/16/15.
//  Copyright (c) 2015 Ryan Ferry. All rights reserved.
//

#include <stdio.h>
#include "myfunction.h"


#define JOULES 1055;
#define BTU_WATT_HR 0.293;

double Total_BTU(double, double);
double Barrels(double);
double Joules(double);
double Watts(double);
void Output(double, double, double, double);

int main() {
    double gallons_used,
        efficiency,
        T_BTU,
        T_Barrels,
        T_joiles,
        T_watts;
    
    printf("Enter the total number of gallons used: \n");
    gallons_used = userInDouble();
    printf("Enter the total Efficiency: \n");
    efficiency = userInDouble();
    
    T_BTU = Total_BTU(gallons_used, efficiency);
    T_Barrels = Barrels(gallons_used);
    T_joiles = Joules(T_BTU);
    T_watts = Watts(T_BTU);
    Output(T_BTU, T_Barrels, T_joiles, T_watts);
    
    return 0;
}

    //Calcluates the total BTU used per gallon of oil
double Total_BTU(double Gallons_Used, double Efficiency){
    double BTU;
    BTU = 1380952.381 * Efficiency * Gallons_Used;
    return BTU;
}

    //Calculates the totla barrels used
double Barrels(double Gallons_Used){
    double Tbarrels;
    Tbarrels = Gallons_Used / 40;
    return Tbarrels;
}

    //Calculates the total amount of joules produced
double Joules(double TotalBTU){
    double tjoules;
    tjoules = TotalBTU * JOULES;
    return tjoules;
}

    //Calculates the total watts
double Watts(double TotalBTU){
    double Twatts;
    Twatts = TotalBTU * BTU_WATT_HR;
    return Twatts;
}

    //prints the output of the calculations
void Output(double Total_BTU, double Total_Barrels, double Total_Joiles, double Total_Watts){
    printf("Total_BTU: %.2f \n", Total_BTU);
    printf("Total_Barrels: %.2f \n", Total_Barrels);
    printf("Total_Joiles: %.2f \n", Total_Joiles);
    printf("Total_Watts: %.2f \n", Total_Watts);
    return;
}
