//
//  function.h
//  Temp Coversion
//
//  Created by Ryan Ferry on 2/16/15.
//  Copyright (c) 2015 Ryan Ferry. All rights reserved.
//

#ifndef __Temp_Coversion__function__
#define __Temp_Coversion__function__

#include <stdio.h>

    //allows the user input and return it as a type double
double userInDouble(){
    double enter;
    scanf(" %lf", &enter);
    return enter;
}

    //allows the user to input and return as a type int
int userInInt(){
    int enter;
    scanf(" %d", &enter);
    return enter;
}

    //allows the user to input and return as a type int
char userInChar(){
    char enter;
    scanf(" %c", &enter);
    return enter;
}

    //converts precent to a decimal
double PrecentToDec(double precent){
    double dec;
    dec = precent / 100;
    return dec;
}

    //converts decimal to a precent
double DecToPrecent(double decimal){
    double precent;
    precent = decimal * 100;
    return precent;
}

#endif /* defined(__Temp_Coversion__function__) */
