#include <windows.h>
#include <time.h>
/*
Controls for program;
Space Bar = roll dice
page up = increase dice size
page down = decrease dice size
arrow keys =  move the dice around the secreen
*/
const wchar_t g_szClassName[] = L"myWindowClass";
void IsEven(HDC hdc, int value, int value1, int value2, int value3, int value4){
	RECT rect;
	int even = 0;

	if (value % 2 == 0) even++;
	if (value1 % 2 == 0) even++;
	if (value2 % 2 == 0) even++;
	if (value3 % 2 == 0) even++;
	if (value4 % 2 == 0) even++;

	rect.left = 350;
	rect.top = 400;
	rect.right = 600;
	rect.bottom = 700;
	
	switch (even){
	case 0:
		DrawText(hdc, L"Even Number of Dice = 0", -1, &rect, DT_WORDBREAK | DT_LEFT); 
		break;
	case 1: 
		DrawText(hdc, L"Even Number of Dice = 1", -1, &rect, DT_WORDBREAK | DT_LEFT); 
		break;
	case 2:
		DrawText(hdc, L"Even Number of Dice = 2", -1, &rect, DT_WORDBREAK | DT_LEFT); 
		break;
	case 3:
		DrawText(hdc, L"Even Number of Dice = 3", -1, &rect, DT_WORDBREAK | DT_LEFT); 
		break;
	case 4: 
		DrawText(hdc, L"Even Number of Dice = 4", -1, &rect, DT_WORDBREAK | DT_LEFT); 
		break;
	case 5: 
		DrawText(hdc, L"Even Number of Dice = 5", -1, &rect, DT_WORDBREAK | DT_LEFT); 
		break;
	}
}
void Circle(HDC hdc, double cx, double cy, double r){
	Ellipse(hdc, (int)(cx - r), (int)(cy - r), (int)(cx + r), (int)(cy + r));
}
void DiceDraw(HDC hdc, int x, int y, double size, int DRed, int DBlue, int DGreen, int DieValue, int SRed, int SGreen, int SBlue){
	HPEN BlkPen;
	HBRUSH Brush;
	HPEN DotPen;
	HBRUSH DotBrush;

	double r = size/10;

	bool Dot0 = false;
	bool Dot1 = false;
	bool Dot2 = false;
	bool Dot3 = false;
	bool Dot4 = false;
	bool Dot5 = false;
	bool Dot6 = false;

	BlkPen = CreatePen(PS_SOLID, 3, RGB(0, 0, 0));
	Brush = CreateSolidBrush(RGB(DRed, DBlue, DGreen));
	DotPen = CreatePen(PS_SOLID, 2, RGB(0, 0, 0));
	DotBrush = CreateSolidBrush(RGB(SRed, SBlue, SGreen));

	SelectObject(hdc, Brush);
	SelectObject(hdc, BlkPen);

	Rectangle(hdc, (int)(x - (size / 2)), (int)(y - (size / 2)), (int)(x + (size / 2)), (int)(y + (size / 2)));

	SelectObject(hdc, DotPen);
	SelectObject(hdc, DotBrush);

	switch (DieValue){
	case 1:
		Dot3 = true; 
		break;
	case 2:
		Dot2 = true;
		Dot4 = true; 
		break;
	case 3:
		Dot2 = true;
		Dot3 = true;
		Dot4 = true; 
		break;
	case 4:
		Dot2 = true;
		Dot4 = true;
		Dot0 = true;
		Dot6 = true;
		break;
	case 5:
		Dot2 = true;
		Dot4 = true;
		Dot0 = true;
		Dot6 = true;
		Dot3 = true;
		break;
	case 6:
		Dot2 = true;
		Dot4 = true;
		Dot0 = true;
		Dot5 = true;
		Dot1= true;
		Dot6 = true;
		break;
	}

	if (Dot0) Circle(hdc, x - (size / 4.0), y - (size / 4.0), r);
	if (Dot1) Circle(hdc, x - (size / 4.0), y, r);
	if (Dot2) Circle(hdc, x - (size / 4.0), y + (size / 4.0), r);
	if (Dot3) Circle(hdc, x, y, r);
	if (Dot4) Circle(hdc, x + (size / 4.0), y - (size / 4.0), r);
	if (Dot5) Circle(hdc, x + (size / 4.0), y, r);
	if (Dot6) Circle(hdc, x + (size / 4.0), y + (size / 4.0), r);

	DeleteObject(BlkPen);
	DeleteObject(Brush);
	DeleteObject(DotPen);
	DeleteObject(DotBrush);
}
LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam){
	PAINTSTRUCT PS;
	HDC hdc;

	static int value = 1;
	static int value1 = 2;
	static int value2 = 3;
	static int value3 = 4; 
	static int value4 = 5;

	static int x = 150;
	static int y = 150;
	static double size = 100;

	static int red = 5;
	static int green = 40;
	static int blue = 180;

	static int red1 = 155;
	static int green1 = 10;
	static int blue1 = 18;

	static int red2 = 60;
	static int green2 = 20;
	static int blue2 = 10;

	static int red3 = 20;
	static int green3 = 40;
	static int blue3 = 70;

	static int red4 = 60;
	static int green4 = 70;
	static int blue4 = 10;

	static int Dred = 10;
	static int Dgreen = 20;
	static int Dblue = 30;

	static int Dred1 = 30;
	static int Dgreen1 = 30;
	static int Dblue1 = 40;

	static int Dred2 = 10;
	static int Dgreen2 = 20;
	static int Dblue2 = 30;

	static int Dred3 = 10;
	static int Dgreen3 = 20;
	static int Dblue3 = 30;

	static int Dred4 = 10;
	static int Dgreen4 = 20;
	static int Dblue4 = 30;

	switch (msg){
	case WM_CHAR:
		if (wParam == 'c'){
			red = rand() % 256;
			green = rand() % 256;
			blue = rand() % 256;

			red1 = rand() % 256;
			green1 = rand() % 256;
			blue1 = rand() % 256;

			red2 = rand() % 256;
			green2 = rand() % 256;
			blue2 = rand() % 256;

			red3 = rand() % 256;
			green3 = rand() % 256;
			blue3 = rand() % 256;

			red4 = rand() % 256;
			green4 = rand() % 256;
			blue4 = rand() % 256;

			Dred = rand() % 256;
			Dgreen = rand() % 256;
			Dblue = rand() % 256;

			Dred1 = rand() % 256;
			Dgreen1 = rand() % 256;
			Dblue1 = rand() % 256;

			Dred2 = rand() % 256;
			Dgreen2 = rand() % 256;
			Dblue2 = rand() % 256;

			Dred3 = rand() % 256;
			Dgreen3 = rand() % 256;
			Dblue3 = rand() % 256;

			Dred4 = rand() % 256;
			Dgreen4 = rand() % 256;
			Dblue4 = rand() % 256;
		}
		if (wParam == VK_SPACE){
			value = rand() % 6 + 1;
			value1 = rand() % 6 + 1;
			value2 = rand() % 6 + 1;
			value3 = rand() % 6 + 1;
			value4 = rand() % 6 + 1;
		}
		InvalidateRect(hwnd, NULL, true);
		break;
	case WM_PAINT:
		hdc = BeginPaint(hwnd, &PS);
		DiceDraw(hdc, x, y, size, red, blue, green, value, Dred, Dblue, Dgreen);
		DiceDraw(hdc, x + 150, y, size, red1, blue1, green1, value1, Dred1, Dblue1, Dgreen1);
		DiceDraw(hdc, x + 300, y, size, red2, blue2, green2, value2, Dred2, Dblue2, Dgreen2);
		DiceDraw(hdc, x + 450, y, size, red3, blue3, green3, value3, Dred3, Dblue3, Dgreen3);
		DiceDraw(hdc, x + 600, y, size, red4, blue4, green4, value4, Dred4, Dblue4, Dgreen4);	
		IsEven(hdc, value, value1, value2, value3, value4);
		break;
	case WM_KEYDOWN:
		if (wParam == VK_RIGHT){
			x += 3;
		}
		else if (wParam == VK_LEFT){
			x -= 3;
		}
		else if (wParam == VK_UP){
			//size *= 1.05;//scale on mac
			y -= 3;
		}
		else if (wParam == VK_DOWN){
			//size /= 1.05; //scale on mac 
			y += 3;
		}
		else if (wParam == VK_PRIOR){
			size *= 1.05;
		}
		else if (wParam == VK_NEXT){
			size /= 1.05;
		}
		InvalidateRect(hwnd, NULL, true);
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hwnd, msg, wParam, lParam);
	}
	return 0;
}
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow){
	WNDCLASSEX wc;
	HWND hwnd;
	MSG Msg;

	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = 0;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = g_szClassName;
	wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	if (!RegisterClassEx(&wc)){
		MessageBox(NULL, L"Window Registration Failed!", L"Error!",
			MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}

	hwnd = CreateWindowEx(WS_EX_CLIENTEDGE, g_szClassName, L"Ryan Ferry: Roll Dice",
		WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, 900, 700,
		NULL, NULL, hInstance, NULL);

	if (hwnd == NULL){
		MessageBox(NULL, L"Window Creation Failed!", L"Error!",
			MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}

	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd);

	while (GetMessage(&Msg, NULL, 0, 0) > 0){
		TranslateMessage(&Msg);
		DispatchMessage(&Msg);
	}
	return Msg.wParam;
}